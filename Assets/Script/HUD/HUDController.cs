﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controlador del HUD
/// </summary>
public class HUDController : MonoBehaviour
{

    /// <summary>
    /// Maneja la vida de los enemigos
    /// </summary>
    public EnemyLifeManager EnemyLifeManager;

    /// <summary>
    /// Cartel de turnos
    /// </summary>
    public Turnlbl turnlbl;

    /// <summary>
    /// Manager del mana del jugador
    /// </summary>
    public ManaManager manaManager;

    /// <summary>
    /// Imagen para rellenar la barra de vida
    /// </summary>
    [SerializeField]
    private Image life = null;


    /// <summary>
    /// Se inicializa el HUD del juego
    /// </summary>
    public void init()
    {
        turnlbl.showTurn("JUGADOR");
        EnemyLifeManager.init();
    }

    /// <summary>
    /// Cambia la barra de porcentaje el porcentaje del mana
    /// </summary>
    /// <param name="percentage"> Valores entre 0 y 1 </param>
    public IEnumerator setLifePercentajeBar(float percentage, float time)
    {
        float t = 0;

        while (life.fillAmount != percentage)
        {
            t += Time.deltaTime / time;
            life.fillAmount = Mathf.Lerp(life.fillAmount, percentage, t);
            yield return null;
        }

        life.fillAmount = percentage;
    }

}