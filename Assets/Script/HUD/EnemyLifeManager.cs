﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Pinta la vida de los enemigos en el HUD de la pantalla
/// </summary>
public class EnemyLifeManager : MonoBehaviour
{

    /// <summary>
    /// Prefab de la barra del enemigo
    /// </summary>
    public GameObject enemyHud;

    /// <summary>
    /// Prefabs instanciados
    /// </summary>
    public List<GameObject> instanciatedPrefabs = new List<GameObject>();


    /// <summary>
    /// Inicializamos el componente
    /// </summary>
    public void init()
    {
        foreach (var enemy in GameManager.Instance.enemyGenerator.enemies)
        {
            instanciatedPrefabs.Add(Instantiate(enemyHud, this.transform));

            Vector2 viewportPoint = Camera.main.WorldToScreenPoint(enemy.transform.position);
            instanciatedPrefabs.Last().transform.position = viewportPoint;
        }
    }

    //Pintamos la vida de los enemigos por pantalla
    void Update()
    {
        if (GameManager.Instance.state == States.INGAME)
        {

            var rect = this.GetComponent<RectTransform>();

            var enemies = GameManager.Instance.enemyGenerator.enemies;

            for (int i = 0; i < enemies.Count; i++)
            {

                // convert screen coords
                Vector2 adjustedPosition = Camera.main.WorldToScreenPoint(enemies[i].transform.position);

                adjustedPosition.x *= rect.rect.width / (float)Camera.main.pixelWidth;
                adjustedPosition.y *= rect.rect.height / (float)Camera.main.pixelHeight;

                // set it
                ((RectTransform)(instanciatedPrefabs[i].transform)).anchoredPosition = adjustedPosition - rect.sizeDelta / 2f;

            }

        }
    }
}
